<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/location_immeubles.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'location_immeubles_description' => 'Plugins qui assembles divers plugins tiers et propose de squelettes pour permettre de gérer la location d’immeubles',
	'location_immeubles_nom' => 'Location d’immeubles',
	'location_immeubles_slogan' => 'Louez vos immeubles'
);
